#!/bin/sh

# YouTube Data API Search URL
base_url='https://www.googleapis.com/youtube/v3/search'

# YouTube Data API Search Parameters - see https://developers.google.com/youtube/v3/docs/search/list#parameters

# 'part' parameter
param_part='part=snippet'
# 'maxResults' parameter
param_max_results='maxResults=5'
# 'query' parameter
param_query="q=$1"
# 'key' parameter
param_key="key=$GOOGLE_API_KEY"
# 'topicId' parameter
param_topic_id='topicId=/m/04rlf' # Limit search to "Music" videos
# 'type' parameter
param_type='type=video'

# 'jq' JSON Filter for transforming YouTube API JSON to Alfred-compatible JSON
# jq_filter='{ items: [.items[] | { type: "default", title: .snippet.title, subtitle: .snippet.description, arg: .id.videoId, icon: { path: "icon.png" } } ] }'
read -r -d '' jq_filter <<'EOF'
{   
  items: [
  .items[] | 
    { 
      type: "default", 
      title: .snippet.title, 
      subtitle: .snippet.description, 
      arg: .id.videoId, 
      variables: {
        title: .snippet.title,
        link: ("https://youtube.com/watch?v=" + .id.videoId)
      },      
      icon: 
        { 
          path: "icon.png" 
        } 
    } 
  ] 
}
EOF

curl -G -s -d -v \
  --data-urlencode "$param_part" \
  --data-urlencode "$param_max_results" \
  --data-urlencode "$param_query" \
  --data-urlencode "$param_key" \
  --data-urlencode "$param_topic_id" \
  --data-urlencode "$param_type" \
  $base_url | \
/usr/local/bin/jq "$jq_filter"