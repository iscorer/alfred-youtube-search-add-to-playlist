#!/bin/sh

status=$(echo "$1" | /usr/local/bin/jq '.status')
title="$title"

if [ $status == "\"success\"" ]
then
	echo "Success! - \"${title}\" added to playlist."
else
	echo "ERROR! - Could not add \"${title}\" to playlist!"
fi