#!/bin/sh

param_title="title=$title"
param_link="link=$link"

curl -G -s -d -v \
  --data-urlencode "$param_title" \
  --data-urlencode "$param_link" \
		$ZAPIER_WEBHOOK_URL